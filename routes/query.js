
const db = require('../../lib/db')
const escape = require('sql-template-strings')


module.exports = async (req, res) => {
  const [locker] = await db.query(escape`
    SELECT *
    FROM casiers
    WHERE connecteur = "'${req.query.connecteur}'"
    LIMIT 1
  `)
  res.status(200).json({ locker })
}
