import Modal from "react-bootstrap/Modal";
import React, { Component } from "react";
import Button from "react-bootstrap/Button";
import { withTranslation } from "../i18n";

class ConnectorsModal extends Component {
    render() {
        return (
            <>
                <Modal className="customModal" show={this.props.show} onHide={this.props.toggleModalShow} centered size="lg">
                    <Modal.Header closeButton>
                        <Modal.Title>{this.props.t("MODAL.TITLE")}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>{this.props.t("MODAL.DESCRIPTION")}</Modal.Body>
                    <Modal.Footer>
                        <Button variant="primary" onClick={this.props.toggleModalShow}>
                            {this.props.t("MODAL.BUTTON")}
                        </Button>
                    </Modal.Footer>
                </Modal>
            </>
        );
    }

    static async getInitialProps() {
        return {
            namespacesRequired: ["common"]
        };
    }
}

export default withTranslation("common")(ConnectorsModal);
