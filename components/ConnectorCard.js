import React, { Component } from "react";
import { Card } from "react-bootstrap";
import Link from "next/link";

class ConnectorCard extends Component {


    render() {
        return (
            <>
            {/* vérifier quel est le connectorType, ensuite vérif dans BDD si casier dispo, si oui /nfcScanRecharge + par URL le num du casier sinon noLocker */}
                <Link href={`/nfcScanRecharge?connecteur=${this.props.connectorType}`} as={`/nfcScanRecharge?connecteur=${this.props.connectorType}`}>
                    <Card className="connectorCard">
                        <Card.Body className="connectorCardBody">
                            <img className="connectorCardImg align-self-center justify-content-center" src={this.props.connectorImg}></img>

                        </Card.Body>
                        <Card.Footer className="connectorCardFooter">{this.props.connectorType}</Card.Footer>
                    </Card>
                </Link>
            </>
        );
    }

  
}

export default ConnectorCard;
