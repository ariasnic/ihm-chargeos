import React, { Component } from "react";
import Card from "react-bootstrap/Card";
import Spinner from "react-bootstrap/Spinner";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faDoorOpen } from "@fortawesome/free-solid-svg-icons";
import { withTranslation } from "../i18n";
import fetch from 'isomorphic-unfetch';
import Router from 'next/router';
import Link from "next/link";

class OpeningPage extends Component {




    render() {
        return (
            <>
                {/* Simulate door opening by redirecting after 5 seconds */}
                {/*<meta httpEquiv="refresh" content={"5; url=" + this.props.nextPage} /> */}
                <h1>{this.props.t("OPENING.WAIT")}</h1>
                <Link href={`${this.props.nextPage}`}>
                <Card className="animationCard" body>
                    <FontAwesomeIcon icon={faDoorOpen} size={"10x"} />
                </Card>
                </Link>
                <Spinner variant="light" animation="border" />
            </>
        );
    }
}

export default withTranslation("common")(OpeningPage);
