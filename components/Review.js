import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import Link from "next/link";
import { withTranslation } from "../i18n";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMeh, faAngry, faSmileBeam } from '@fortawesome/free-solid-svg-icons';

class Review extends React.Component {
    constructor(props) {
        super(props);
    }

    sendReview(type) {
        //Think of a solution to export counts of votes at the end of the day
        switch (type) {
            case "bad":
                break;
            case "meh":
                break;
            case "great":
                break;
        }
    }

    render() {
        return (
            <>
                <h1>{this.props.t("QUICK_REVIEW.TITLE")}</h1>
                <Row className="quickCenter">
                    <Link href="/moreReview">
                        <Col className="quickCenter">
                            <span className="bad-review behindSmiley" onClick={() => this.sendReview("bad")}>
                                <FontAwesomeIcon icon={faAngry} size={"7x"} />
                            </span>
                        </Col>
                    </Link>
                    <Link href="/moreReview">
                        <Col className="quickCenter">
                            <span className="meh-review behindSmiley" onClick={() => this.sendReview("meh")}>
                                <FontAwesomeIcon icon={faMeh} size={"7x"} />
                            </span>
                        </Col>
                    </Link>
                    <Link href="/positiveReview">
                        <Col className="quickCenter">
                            <span className="great-review behindSmiley" onClick={() => this.sendReview("great")}>
                                <FontAwesomeIcon icon={faSmileBeam} size={"7x"} />
                            </span>
                        </Col>
                    </Link>
                </Row>
            </>


        );
    }

    static async getInitialProps() {
        return {
            namespacesRequired: ["common"]
        };
    }
}

export default withTranslation("common")(Review);