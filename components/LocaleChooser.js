import React, { Component } from "react";
import { Col, Container, Row } from "react-bootstrap";
import { i18n } from "../i18n";
class LocaleChooser extends Component {
    constructor(props) {
        super(props);
        this.state = { selectedItem: "" };
    }

    componentDidMount() {
        this.setState({ selectedItem: localStorage.getItem("selectedItem") });
    }

    render() {
        return (
            <>
                <Container className="sticky sticky-right">
                    <Row className="sticky-row">
                        <Col className="sticky-col">
                            <img src="/country_fr.png" id="fr" onClick={e => this.changeLanguage(e)} className={this.state.selectedItem !== "fr" ? "gray" : null} />
                            <img src="/country_uk.png" id="en" onClick={e => this.changeLanguage(e)} className={this.state.selectedItem !== "en" ? "gray" : null} />
                        </Col>
                    </Row>
                </Container>
            </>
        );
    }

    changeLanguage(event) {
        i18n.changeLanguage(event.currentTarget.id);
        localStorage.setItem("selectedItem", event.currentTarget.id);
        this.setState({ selectedItem: event.currentTarget.id });
    }
}

export default LocaleChooser;
