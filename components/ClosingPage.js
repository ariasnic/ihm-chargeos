import React, { Component } from "react";
import Card from "react-bootstrap/Card";
import Spinner from "react-bootstrap/Spinner";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faDoorClosed } from "@fortawesome/free-solid-svg-icons";
import { withTranslation } from "../i18n";
import Link from "next/link";

class ClosingPage extends Component {
    render() {
        return (
            <>
                {/* Simulate door opening by redirecting after 5 seconds */}
                {/*<meta httpEquiv="refresh" content={"5; url=" + this.props.nextPage} />*/}
                <h1>{this.props.t("CLOSING.WAIT")}</h1>
                <Link href={`${this.props.nextPage}`}>
                <Card className="animationCard" body>
                    <FontAwesomeIcon icon={faDoorClosed} size={"10x"} />
                </Card>
                </Link>
                <Spinner variant="light" animation="border" />
            </>
        );
    }

    static async getInitialProps() {
        return {
            namespacesRequired: ["common"]
        };
    }
}

export default withTranslation("common")(ClosingPage);
