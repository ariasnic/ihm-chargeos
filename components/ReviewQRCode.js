import {useEffect} from 'react';
import { Container, Col, Row } from 'react-bootstrap';
import { withTranslation } from "../i18n";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMeh, faAngry } from '@fortawesome/free-solid-svg-icons';
import {useRouter} from 'next/router';


const ReviewQRCode = (props) => {
    const router = useRouter();
    
    useEffect (() => {
        setTimeout(
            () => router.push("/"), 10000
        );
    });

    return (
        <Container className="mainContainer">
            <Row className="mainRow quickCenter pickupContainer">
                <Col className="quickCenterCol">
                    <Row className="quickCenter">
                        <Col className="quickCenter">
                            <span className="bad-review behindSmiley">
                                <FontAwesomeIcon icon={faAngry} size={"7x"} />
                            </span>
                        </Col>
                        <Col className="quickCenter">
                            <span className="meh-review behindSmiley">
                                <FontAwesomeIcon icon={faMeh} size={"7x"} />
                            </span>
                        </Col>
                    </Row>
                    <Row className="titleArea">
                        <h1>{props.t("MORE_REVIEW.TITLE")}</h1>
                    </Row>
                    <Row className="quickCenter">
                        <a>
                            <img src='https://www.unitag.io/qreator/generate?crs=xnjFkEn%252FP85fCPDXJ%252FXXKnPnKU%252FtWVh9E7ei8Ex%252BR4XsTvus59MiRl4OtJ5Y%252F3aRXopA7Qn4wJ6m3qLfsP4IWv39ocSd3mMczmj1AuyiW6K%252F58n8n8s5NK61vAUi6GUR9QhYs1xUoNWG3PC4owAgU1Q%252FHThW3FIfdeEUqZ%252BlJgc%253D&crd=fhOysE0g3Bah%252BuqXA7NPQy31KAHjkKet3fjQYdytp%252FnLAzyOuwUsW0LYSwZY2bhPcqsUc9%252B1VO1gPC6tgqXcbRkJhChfVilQy5LLsQSyXzs%253D' alt='QR Code' />
                        </a>
                    </Row>
                </Col>
            </Row>
        </Container>
    );
}

ReviewQRCode.getInitialProps = async () => {
    return {
        namespacesRequired: ["common"]
    };
}

export default withTranslation("common")(ReviewQRCode);