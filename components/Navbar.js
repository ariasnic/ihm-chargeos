import React, { Component } from "react";
import { Col, Container, Row, Button } from "react-bootstrap";
import { withTranslation } from "../i18n";
import Link from "next/link";
class NavBar extends Component {
    constructor(props) {
        super(props);
        this.state = { selectedItem: "fr" };
    }

    render() {
        return (
            <>
                <Container className="sticky sticky-left">
                    <Row className="sticky-row">
                        <Col className="sticky-col">
                            <Link href="/">
                                <Button id="homeBtn" variant="light">
                                    <img id="homeIcon" src="/home.svg" />
                                </Button>
                            </Link>
                        </Col>
                    </Row>
                </Container>
            </>
        );
    }

    static async getInitialProps() {
        return {
            namespacesRequired: ["common"]
        };
    }
}

export default withTranslation("common")(NavBar);
