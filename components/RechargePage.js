import React, { Component } from "react";
import { Container, Row, Col } from "react-bootstrap";
import NavBar from "../components/Navbar";

class RechargePage extends Component {
    render() {
        return (
            <>
                <NavBar />
                <Container className="mainContainer">
                    <Row className="mainRow">
                        <Col className="rechargeColumn">{this.props.children}</Col>
                    </Row>
                </Container>
            </>
        );
    }
}

export default RechargePage;
