import React, { Component } from "react";
import { Col, Container, Row, Card } from "react-bootstrap";
import { withTranslation } from "../i18n";
import Link from "next/link";
import NavBar from "../components/Navbar";

class Home extends Component {
    render() {
        return (
            <>
                <NavBar />
                <Container className="mainContainer">
                    <Row className="mainRow">
                        <Col className="rechargeColumn">
                            <Card className="animationCard" body>
                                <img src="/battery_charging.svg" />
                            </Card>
                            <h1>{this.props.t("HOME.CHARGE")}</h1>
                        </Col>
                        <Col className="retrieveColumn">
                            <Card className="animationCard" body>
                                <img src="/battery_full.svg" />
                            </Card>
                            <h1>{this.props.t("HOME.RETRIEVE")}</h1>
                        </Col>
                    </Row>
                </Container>
                <Container className="eventHandler">
                    <Row className="rowHandler">
                        <Link href="/connectors">
                            <Col className="leftHandler"></Col>
                        </Link>
                        <Link href="/nfcScanRetrieve">
                            <Col className="rightHandler"></Col>
                        </Link>
                    </Row>
                </Container>
            </>
        );
    }

    static async getInitialProps() {
        return {
            namespacesRequired: ["common"]
        };
    }
}

export default withTranslation("common")(Home);
