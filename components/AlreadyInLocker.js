import React, { Component } from "react";
import Card from "react-bootstrap/Card";
import Spinner from "react-bootstrap/Spinner";
import { withTranslation } from "../i18n";
import Link from "next/link";
import { Button } from "react-bootstrap";

class AlreadyInLocker extends Component {
    render() {
        return (
            <>
                <div className="container text-center">
                    <h1>{this.props.t("ALREADY_IN_CHARGE.FOUND_IN")}</h1>
                    <h1 style={{ marginTop: "1.5rem" }}>{this.props.t("ALREADY_IN_CHARGE.PICK_UP")}</h1>
                </div>
                <Link href="/">
                    <div className="container text-center spaced" style={{ marginTop: "3rem" }}>
                        <Button variant="light" size="lg">
                            {this.props.t("ALREADY_IN_CHARGE.GO_BACK")}
                        </Button>
                    </div>
                </Link>
            </>
        );
    }

    static async getInitialProps() {
        return {
            namespacesRequired: ["common"]
        };
    }
}

export default withTranslation("common")(AlreadyInLocker);