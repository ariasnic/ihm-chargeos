-- phpMyAdmin SQL Dump
-- version 4.9.3
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le :  jeu. 09 juil. 2020 à 13:26
-- Version du serveur :  5.7.26
-- Version de PHP :  7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `chargeos`
--

-- --------------------------------------------------------

--
-- Structure de la table `casiers`
--

CREATE TABLE `casiers` (
  `id` int(11) NOT NULL,
  `connecteur` varchar(255) NOT NULL,
  `etat` int(11) NOT NULL,
  `UID` varchar(50) DEFAULT NULL,
  `timeDepot` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `casiers`
--

INSERT INTO `casiers` (`id`, `connecteur`, `etat`, `UID`, `timeDepot`) VALUES
(1, 'iphone', 0, NULL, NULL),
(2, 'iphone', 0, NULL, NULL),
(3, 'micro_usb', 0, NULL, NULL),
(4, 'micro_usb', 0, NULL, NULL),
(5, 'usb_c', 0, NULL, NULL),
(6, 'usb_c', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `rechargements`
--

CREATE TABLE `rechargements` (
  `id` int(11) NOT NULL,
  `UID` varchar(50) NOT NULL,
  `casier` varchar(11) NOT NULL,
  `timeDepot` bigint(20) NOT NULL,
  `timeRecup` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `rechargements`
--

INSERT INTO `rechargements` (`id`, `UID`, `casier`, `timeDepot`, `timeRecup`) VALUES
(1, 'A', '2', 1588609089174, 1588609095513),
(2, 'A', '2', 1591370274664, 1591370328946),
(3, 'A', '2', 1593597960361, 1593597980336),
(4, 'A', '2', 1593600790595, 1593601300516),
(5, 'A', '2', 1593601314966, 1593601346751),
(6, 'A', '2', 1593618473313, 1593618525666),
(7, 'A', '2', 1594056113270, 1594056133437),
(8, 'A', '2', 1594056294089, 1594056360236);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateurs`
--

CREATE TABLE `utilisateurs` (
  `UID` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `utilisateurs`
--

INSERT INTO `utilisateurs` (`UID`) VALUES
('A'),
('B'),
('C');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `casiers`
--
ALTER TABLE `casiers`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `rechargements`
--
ALTER TABLE `rechargements`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  ADD PRIMARY KEY (`UID`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `rechargements`
--
ALTER TABLE `rechargements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
