# ariel

## Prerequisites

You need Nodejs to build and start your application. Download it here: https://nodejs.org/en/
Launch the following commands (root folder) to start the application, in development / production mode:

## Production

-   npm install
-   npm run build
-   npm run start

## Development

-   npm install
-   npm run dev

In the both cases, the application should run at http://localhost:3000/.
