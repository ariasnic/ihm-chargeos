const express = require("express");
const next = require("next");
const nextI18NextMiddleware = require("next-i18next/middleware").default;

const nextI18next = require("./i18n");

const port = process.env.PORT || 3000;
const app = next({ dev: process.env.NODE_ENV !== "production" });
const handle = app.getRequestHandler();


const { allumerLed, eteindreLed } = require( '/home/pi/chargeos/ihm-chargeos/pages/mecatronique.js' ); //à modifier 


(async () => {
    await app.prepare();
    const server = express();
    const httpServer = require('http').createServer(server);
    server.use(nextI18NextMiddleware(nextI18next));

    server.get("*", (req, res) => handle(req, res));

    await httpServer.listen(port);
    console.log(`> Ready on http://localhost:${port}`); // eslint-disable-line no-console

    const io = require('socket.io')(httpServer);
    io.on('connection', (socket) => { 
    	socket.on('light', function(data) { //événement que l'on attend: light
    		  	console.log( 'Received light event.' );
    		  	allumerLed();	
    	});

    });


 //    process.on('SIGINT', function () { //on ctrl+c
 //    	console.log( 'Received light event.' );
	// 	eteindreLed();
	// });

})();


//Sans socket.io

// const express = require("express");
// const next = require("next");
// const nextI18NextMiddleware = require("next-i18next/middleware").default;

// const nextI18next = require("./i18n");

// const port = process.env.PORT || 3000;
// const app = next({ dev: process.env.NODE_ENV !== "production" });
// const handle = app.getRequestHandler();

// (async () => {
//     await app.prepare();
//     const server = express();
    
//     server.use(nextI18NextMiddleware(nextI18next));

//     server.get("*", (req, res) => handle(req, res));

//     await server.listen(port);
//     console.log(`> Ready on http://localhost:${port}`); // eslint-disable-line no-console

// })();