const db = require('../../lib/db')
const escape = require('sql-template-strings')




module.exports = async (req, res) => {
  var [inCharge] = await db.query(escape`
    SELECT COUNT(*) AS phoneCharging
    FROM casiers
    WHERE UID = ${req.query.UID} AND etat=1
  `)
  if (inCharge.phoneCharging==1) {
    var [locker] = await db.query(escape`
      SELECT id
      FROM casiers
      WHERE UID = ${req.query.UID} AND etat=1
      LIMIT 1
    `)
    res.status(200).json( {inCharge, locker} )
  } else{
    res.status(200).json( {inCharge} )
  }
  
}