const db = require('../../lib/db')
const escape = require('sql-template-strings')


module.exports = async (req, res) => {
  const timeRecup = Date.now()
  const [infoLocker] = await db.query(escape`
    SELECT timeDepot
    FROM casiers
    WHERE id = ${req.query.locker}
  `)
  await db.query(escape`
     UPDATE casiers SET UID=NULL, etat=0, timeDepot=NULL
     WHERE id = ${req.query.locker} 
  `)
  await db.query(escape`
      INSERT INTO rechargements (UID, casier, timeDepot, timeRecup)
      VALUES (${req.query.UID}, ${req.query.locker}, ${infoLocker.timeDepot}, ${timeRecup} )
    `)
  res.status(200).json({message: 1})
  }
  

