
const db = require('../../lib/db')
const escape = require('sql-template-strings')




module.exports = async (req, res) => {
  var [libre] = await db.query(escape`
    SELECT COUNT(*) AS nbLibre
    FROM casiers
    WHERE connecteur = ${req.query.connecteur} AND etat=0
  `)
  if (libre.nbLibre>0) {
    var [locker] = await db.query(escape`
      SELECT id
      FROM casiers
      WHERE connecteur = ${req.query.connecteur} AND etat=0
      LIMIT 1
    `)
    res.status(200).json( {libre, locker} )
  } else{
    res.status(200).json( {libre} )
  }
  
}




  





