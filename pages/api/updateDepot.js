const db = require('../../lib/db')
const escape = require('sql-template-strings')




module.exports = async (req, res) => {
  const time = Date.now()
  await db.query(escape`
    UPDATE casiers SET UID=${req.query.UID}, etat=1, timeDepot=${time}
    WHERE id = ${req.query.locker} 
  `)
  var [isFirstTime] = await db.query(escape`
    SELECT COUNT(*) AS nbUID
    FROM utilisateurs
    WHERE UID = ${req.query.UID}
  `)
  //si 1ere utilisation, on enregistre l'UID dans 'utilisateurs'
  if (isFirstTime.nbUID==0) {
    await db.query(escape`
      INSERT INTO utilisateurs (UID)
      VALUES (${req.query.UID})
    `)
   }
    res.status(200).json({message: 1})
  }
  

