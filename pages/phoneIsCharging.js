import RechargePage from "../components/RechargePage";
import {useEffect} from 'react';
import React, { Component } from "react";
import { withTranslation } from "../i18n";
import {Container, Row, Col} from 'react-bootstrap';
import {useRouter} from 'next/router';
import Router from 'next/router';

class PhoneIsCharging extends Component {

    static async getInitialProps({query}) {
        const pageRequest = `http://localhost:3000/api/updateDepot?locker=${query.locker}&UID=${query.UID}`
        const res = await fetch(pageRequest)
        const json = await res.json()
        console.log(json)

     return {
            namespacesRequired: ["common"]
        }
    }    
    
    render() {
        return (
            <>
                <meta httpEquiv="refresh" content="7; url=/" />
                <RechargePage>
                    <Container>
                        <Row className="mainRow">
                            <Col className="quickCenter">
                                <h1>{this.props.t("CONFIRM_RECHARGE.TITLE")}</h1>
                            </Col>
                        </Row>
                    </Container>
                </RechargePage>
            </>
        );
    }
}



export default withTranslation("common")(PhoneIsCharging)
