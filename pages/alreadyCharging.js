import AlreadyInLocker from "../components/AlreadyInLocker";
import RechargePage from "../components/RechargePage";

export default () => {
    return (
        <>
            <RechargePage>
                <AlreadyInLocker />
            </RechargePage>
        </>
    );
};
