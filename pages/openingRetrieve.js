import RetrievePage from "../components/RetrievePage";
import OpeningPage from "../components/OpeningPage";
import fetch from 'isomorphic-unfetch';
import Link from "next/link";
import React, { Component } from "react";
import { withTranslation } from "../i18n";
import Router from 'next/router';

class openingRetrieve extends Component {

	static async getInitialProps({query}) {
	   
		const pageRequest = `http://localhost:3000/api/findLocker?UID=${query.UID}`
	    const res = await fetch(pageRequest)
	    const json = await res.json()
	    console.log(json.inCharge)
	    console.log(json.locker)

	    if(json.inCharge.phoneCharging==0){
        	return{
		      	phoneCharging: json.inCharge.phoneCharging,
		        namespacesRequired: ["common"]
	        }
    	} else {
	        return{
	            locker: json.locker.id,
		    	UID: query.UID,
		      	phoneCharging: json.inCharge.phoneCharging,
		        namespacesRequired: ["common"]
	        }
  		}
	}

	componentDidMount(){
		if(this.props.phoneCharging==0){
	          Router.push('/noPhoneCharging')
	     }
	}       

	    render() {
	        return (
	        <>
	            <RetrievePage>
                	<OpeningPage nextPage={`/retrievePhone?locker=${this.props.locker}&UID=${this.props.UID}`}/>
            	</RetrievePage>
	        </>
	    	);
	    }

    
}

export default withTranslation("common")(openingRetrieve);

