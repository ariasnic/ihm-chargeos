import "bootstrap/dist/css/bootstrap.min.css";
import App from "next/app";
import Head from "next/head";
import React from "react";
import { appWithTranslation } from "../i18n";
import LocaleChooser from "../components/LocaleChooser";

class Ariel extends App {
    render() {
        const { Component, pageProps } = this.props
        return (
            <>
                <Head>
                    <title>Ariel</title>
                </Head>
                <LocaleChooser />
                <Component {...pageProps} />
            </>
        );
    }
}

export default appWithTranslation(Ariel);
