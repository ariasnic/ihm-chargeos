import React, { Component } from "react";
import Home from "../components/Home";

class Main extends Component {
    render() {
        return <Home />;
    }

    static async getInitialProps() {
        return {
            namespacesRequired: ["common"]
        };
    }
}
export default Main;
