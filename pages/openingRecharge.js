import RechargePage from "../components/RechargePage";
import OpeningPage from "../components/OpeningPage";
import fetch from 'isomorphic-unfetch';
import Link from "next/link";
import React, { Component } from "react";
import { withTranslation } from "../i18n";
import Router from 'next/router';



class openingRecharge extends Component {

	static async getInitialProps({query}) {
	   
		const pageRequest = `http://localhost:3000/api/checkUID?UID=${query.UID}`
	    const res = await fetch(pageRequest)
	    const json = await res.json()
	    console.log(json.user)


	    return{
	    	locker: query.locker,
	    	UID: query.UID,
	      	alreadyIn: json.user.alreadyIn,
	        namespacesRequired: ["common"]
	    }
	}

	componentDidMount(){
		// var mecatro = require ("./api/mecatronique.js");
		if(this.props.alreadyIn==1){
	          Router.push('/alreadyCharging')
	    // } else if(this.props.alreadyIn==0){
	    // 	const casier = this.props.locker % 15;
	    // 	const etage = Math.trunc(this.props.locker/15 + 1);
	    // 	mecatro.presenterCasier(etage, casier);
	     }
	}       

	    render() {
	        return (
	        <>
	            <RechargePage>
	                <OpeningPage nextPage={`/plugPhone?locker=${this.props.locker}&UID=${this.props.UID}`}/>
	            </RechargePage>
	        </>
	    );
	    }

    
}

export default withTranslation("common")(openingRecharge);
