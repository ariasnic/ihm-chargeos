import RetrievePage from "../components/RetrievePage";
import React, { Component } from "react";
import { Card } from "react-bootstrap";
import { withTranslation } from "../i18n";
import Link from "next/link";

class NfcScanRetrieve extends Component {
    render() {
        return (
            <>
                {/* Simulate NFC scan by redirecting after 5 seconds */}
                {/*<meta httpEquiv="refresh" content="5; url=/openingRetrieve" />*/}
                <RetrievePage>
                    <img className="nfcIndicator" src="/chalk_arrow.png" />
                    <h1>{this.props.t("NFC.SCAN")}</h1>
                    <Link href={`/openingRetrieve?UID=A`}>
                    <Card className="animationCard" body>
                        <img src="/nfc.svg" />
                    </Card>
                    </Link>
                </RetrievePage>
            </>
        );
    }

    static async getInitialProps() {
        return {
            namespacesRequired: ["common"]
        };
    }
}

export default withTranslation("common")(NfcScanRetrieve);
