import RetrievePage from "../components/RetrievePage";
import { Card } from "react-bootstrap";
import React, { Component } from "react";
import { Col, Container, Row } from "react-bootstrap";
import { withTranslation } from "../i18n";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMobile } from "@fortawesome/free-solid-svg-icons";
import { useEffect } from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import { Button } from "react-bootstrap";
import ButtonToolbar from 'react-bootstrap/ButtonToolbar'
import ButtonGroup from 'react-bootstrap/ButtonGroup'


class RetrievePhone extends Component {
    // const router = useRouter();

    // useEffect(() => {
    //     setTimeout(() => router.push("/quickReview"), 7000);
    // });


    async updateRecup(){
        const pageRequest = `http://localhost:3000/api/updateRecup?locker=${this.props.locker}&UID=${this.props.UID}`
        const res = await fetch(pageRequest)
        const json = await res.json()
        console.log(json)
    }

    render() {
        return (
        <>
            <RetrievePage>
                <h1>{this.props.t("RETRIEVE_PHONE.TITLE")}</h1>
                <Card className="animationCard" body>
                    <FontAwesomeIcon icon={faMobile} size={"10x"} />
                    <img src="/battery_full.svg" />
                </Card>
                
                <ButtonToolbar>
                <ButtonGroup>
                    <Link href="/">
                        <div className="container text-center spaced" style={{ marginTop: "3rem" }}>
                            <Button variant="light" size="lg">
                                Laisser Charger
                            </Button>
                        </div>
                    </Link>
                </ButtonGroup>
                <ButtonGroup>
                    <Link href="/">
                        <div onClick={()=> this.updateRecup()} className="container text-center spaced" style={{ marginTop: "3rem" }}>
                            <Button variant="light" size="lg">
                                J'ai récupéré 
                            </Button>
                        </div>
                    </Link>
                </ButtonGroup>
               </ButtonToolbar>
            </RetrievePage>
        </>
        );
    }


  static async getInitialProps({query}) {
        return {
            UID: query.UID,
            locker: query.locker,
            namespacesRequired: ["common"]
        };
    }

};

export default withTranslation("common")(RetrievePhone);
