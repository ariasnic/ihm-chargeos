import RechargePage from "../components/RechargePage";
import React, { Component } from "react";
import { Card } from "react-bootstrap";
import { withTranslation } from "../i18n";
import Link from "next/link";
import fetch from 'isomorphic-unfetch';
import Router from 'next/router';


class NfcScanRecharge extends Component {

    static async getInitialProps({query}) {
       
        const pageRequest = `http://localhost:3000/api/chooseLocker?connecteur=${query.connecteur}`
        const res = await fetch(pageRequest)
        const json = await res.json()

        console.log(json.libre)
        console.log(json.locker)

        

         if(json.libre.nbLibre==0){
            return{
                nbLibre: json.libre.nbLibre,
                namespacesRequired: ["common"]
            }
        } else {
            return{
                nbLibre: json.libre.nbLibre,
                locker: json.locker.id,
                namespacesRequired: ["common"]
            }
        }
    }    

componentDidMount(){
        if(this.props.nbLibre==0){
              Router.push('/noLocker')
         }
    }           

    render() {
        return (
            <>
                {/* Simulate NFC scan by redirecting after 5 seconds */}
                {/*<meta httpEquiv="refresh" content="5; url=/openingRecharge" /> */}
                <RechargePage>
                    <img className="nfcIndicator" src="/chalk_arrow.png" />
                    <h1>{this.props.t("NFC.SCAN")}</h1>
                    <Link href={`/openingRecharge?locker=${this.props.locker}&UID=A`}>
                    <Card className="animationCard" body>
                        <img src="/nfc.svg" />
                    </Card>
                    </Link>
                </RechargePage>
            </>
        );
    }

    
}

export default withTranslation("common")(NfcScanRecharge);
