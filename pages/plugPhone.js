import { Card } from "react-bootstrap";
import { withTranslation } from "../i18n";
import { useEffect } from "react";
import React, { Component } from "react";
import { useRouter } from "next/router";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMobileAlt } from "@fortawesome/free-solid-svg-icons";
import RechargePage from "../components/RechargePage";
import Link from "next/link";


class PlugPhone extends Component {
    // const router = useRouter();

    // useEffect(() => {
    //     setTimeout(() => router.push("/closingRecharge"), 7000);
    // });

    static async getInitialProps({query}) {
        return {
            locker: query.locker,
            UID: query.UID,
            namespacesRequired: ["common"]
        }
    }
        //récupérer info de quand le casier est fermé pour passer à l'étape suivante
    render() {
        return (
            <>
            <RechargePage>
                <h1>{this.props.t("CONNECT_PHONE.TITLE")}</h1>
                <Link href={`/closingRecharge?locker=${this.props.locker}&UID=${this.props.UID}`}>
                <Card className="animationCard" body>
                    <FontAwesomeIcon icon={faMobileAlt} size={"10x"} />
                    <img src="/battery_charging.svg" />
                </Card>
                </Link>
            </RechargePage>
        </>
        );
    }
}



export default withTranslation("common")(PlugPhone);
