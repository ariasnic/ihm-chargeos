import RetrievePage from "../components/RetrievePage";
import Review from "../components/Review";

export default () => {
    return (
        <>
            <RetrievePage>
                <Review />
            </RetrievePage>
        </>
    );
};
