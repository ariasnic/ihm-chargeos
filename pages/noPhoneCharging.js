import NoPhoneIn from "../components/NoPhoneIn";
import RetrievePage from "../components/RetrievePage";

export default () => {
    return (
        <>
            <RetrievePage>
                <NoPhoneIn />
            </RetrievePage>
        </>
    );
};
