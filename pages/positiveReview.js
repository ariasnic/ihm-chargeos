import RetrievePage from "../components/RetrievePage";
import {useEffect} from 'react';
import { withTranslation } from "../i18n";
import {Container, Row, Col} from 'react-bootstrap';
import {useRouter} from 'next/router';


const PositiveReview = (props) => {
    const router = useRouter();

    useEffect (() => {
        setTimeout(
            () => router.push("/"), 7000
        );
    });

    return (
        <RetrievePage>
            <h1>{props.t("FINISHED_REVIEW.TITLE")}</h1>
        </RetrievePage>
    );
};

PositiveReview.getInitialProps = async () => {
    return {
        namespacesRequired: ["common"]
    };
}

export default withTranslation("common")(PositiveReview);