import RetrievePage from "../components/RetrievePage";
import ReviewQRCode from "../components/ReviewQRCode";

export default () => {
    return (
        <>
            <RetrievePage>
                <ReviewQRCode />
            </RetrievePage>
        </>
    );
};
