import RechargePage from "../components/RechargePage";
import ClosingPage from "../components/ClosingPage";
import React, { Component } from "react";
import { withTranslation } from "../i18n";

class ClosingRecharge extends Component {

	static async getInitialProps({query}) {
        return {
            locker: query.locker,
            UID: query.UID,
            namespacesRequired: ["common"]
        }
    }

    render(){
    	return (
	        <>
	            <RechargePage>
	                <ClosingPage nextPage={`/phoneIsCharging?locker=${this.props.locker}&UID=${this.props.UID}`}/>
	            </RechargePage>
	        </>
    	);
    }
    
};

export default withTranslation("common")(ClosingRecharge);

