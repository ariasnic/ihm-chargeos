import RetrievePage from "../components/RetrievePage";
import OpeningPage from "../components/OpeningPage";
import fetch from 'isomorphic-unfetch';
import Link from "next/link";
import React, { Component } from "react";
import { withTranslation } from "../i18n";
import Router from 'next/router';
import { Button } from "react-bootstrap";
import io from 'socket.io-client';


class testMecatronique extends Component {

	static async getInitialProps({query}) {
					   
					   // let mecatro = require ("./mecatronique");

	   return {
            namespacesRequired: ["common"]
        };
		
	}   

	    render() {

	    	// var allumer = document.getElementById( 'allumer' );

	    	

	    	var isConnectionActive = false;

			// connect to the Web Socket server
			var connection = io();

			// when connection is established 
			connection.on( 'connect', () => {
			  isConnectionActive = true;
			} );

			connection.on( 'disconnect', () => {
			  isConnectionActive = false;
			} );

			// WebSocket event emitter function
			var emitEvent = function( event ) {
				if( ! isConnectionActive ) {
			    	return alert( 'Server connection is closed!' );
			  	}

				connection.emit( 'light', );

				 

			}

			// add event listeners on button
			// allumer.addEventListener( 'click', emitEvent );


	        return (
	        <>

	        			<div onClick={()=> emitEvent()} className="container text-center spaced" style={{ marginTop: "3rem" }} id="allumer">>
                            <Button variant="light" size="lg">
                                Allumer Led
                            </Button>
                        </div>

                        // <div onClick={()=> mecatro.attenteFermeture(1)} className="container text-center spaced" style={{ marginTop: "3rem" }}>
                        //     <Button variant="light" size="lg">
                        //         Attente fermeture
                        //     </Button>
                        // </div>

                        // <div onClick={()=> mecatro.ouvrirPorte(1)} className="container text-center spaced" style={{ marginTop: "3rem" }}>
                        //     <Button variant="light" size="lg">
                        //         Ouvrir Porte 
                        //     </Button>
                        // </div>

                        // <div onClick={()=> mecatro.rotationTouret(5)} className="container text-center spaced" style={{ marginTop: "3rem" }}>
                        //     <Button variant="light" size="lg">
                        //         Rotation touret
                        //     </Button>
                        // </div>
                     
	        </>
	    	);
	    }

    
}

export default withTranslation("common")(testMecatronique);

