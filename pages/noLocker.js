import NoLockerAvailable from "../components/NoLockerAvailable";
import RechargePage from "../components/RechargePage";

export default () => {
    return (
        <>
            <RechargePage>
                <NoLockerAvailable />
            </RechargePage>
        </>
    );
};
