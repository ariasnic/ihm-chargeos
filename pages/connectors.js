import ConnectorCard from "../components/ConnectorCard";
import RechargePage from "../components/RechargePage";
import ConnectorsModal from "../components/ConnectorsModal";
import { CardGroup, Button } from "react-bootstrap";
import Link from "next/link";
import React, { Component } from "react";
import { withTranslation } from "../i18n";
import fetch from 'isomorphic-unfetch';



class Connectors extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalShow: false
        };
        this.toggleModalShow = this.toggleModalShow.bind(this);
    }

    render() {
        return (
            <>
                {this.showModal()}
                <RechargePage>
                    <h1>{this.props.t("CONNECTORS.TITLE")}</h1>
                    <CardGroup className="connectorCardGroup">
                        <ConnectorCard connectorType="Type C" connectorImg="/typec.png"></ConnectorCard>
                        <ConnectorCard connectorType="Micro USB" connectorImg="/micro_usb.png"></ConnectorCard>
                        <ConnectorCard connectorType="iPhone" connectorImg="/iphone.png"></ConnectorCard>
                    </CardGroup>
                    <Button variant="secondary" size="lg" onClick={() => this.toggleModalShow()}>
                        {this.props.t("CONNECTORS.BUTTON")}
                    </Button>
                </RechargePage>
            </>
        );
    }

    toggleModalShow() {
        this.setState({
            modalShow: !this.state.modalShow
        });
    }

    showModal() {
        return <ConnectorsModal show={this.state.modalShow} toggleModalShow={this.toggleModalShow} />;
    }

    static async getInitialProps() {
        return {
            namespacesRequired: ["common"]
        };
       
    }
}

export default withTranslation("common")(Connectors);
